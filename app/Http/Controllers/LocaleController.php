<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LocaleController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param $locale
     * @return \Illuminate\Http\Response
     */
    public function setlocale($locale)
    {
        Session::put('locale', $locale);
        app()->setLocale(Session::get('locale'));

        return Redirect::back();
    }
}
