<?php

namespace App\Http\Controllers;

use App\NewsletterSubscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Newsletter;

class NewsletterController extends Controller
{
    /**
     * Subscribes the user to our newsletter.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $is_subscribed = false;

        if (Auth::check()) {
            $user = Auth::user();
            $subscriber = NewsletterSubscriber::where('email', $user->email)->first();

            if ($subscriber !== null) {
                $is_subscribed = true;
            }
        }

        return view('newsletter/home')->with('is_subscribed', $is_subscribed);
    }

    /**
     * Subscribes the user to our newsletter.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function subscribe(Request $request)
    {
        $request->validate([
            'email' => 'email|required|max:255'
        ]);
        $email = $request->input('email');

        // Prevent multiple subscriptions error.
        if (!NewsletterSubscriber::where('email', $email)->exists()) {
            $subscriber = new NewsletterSubscriber;
            $subscriber->email = $email;
            $subscriber->save();
        }

        // Disabled because I don't have API key.
        // Newsletter::subscribe($email);

        return redirect()->route('newsletter.subscribed');
    }

    /**
     * Unsubscribes the user from our newsletter.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function unsubscribe(Request $request)
    {
        $request->validate([
            'email' => 'email|required|max:255'
        ]);
        $email = $request->input('email');

        NewsletterSubscriber::where('email', $email)->delete();

        // Disabled because I don't have API key.
        // Newsletter::unsubscribe($email);

        return redirect()->route('newsletter.unsubscribed');
    }

    public function subscribed()
    {
        return view('newsletter/subscribed');
    }

    public function unsubscribed()
    {
        return view('newsletter/unsubscribed');
    }
}
