<?php

return [

    'email' => 'Emailová adresa',

    'subscribe' => 'Odoberať',
    'unsubscribe' => 'Zrušiť odber',
    'newsletter' => 'Newsletter',

    'notlogged' => 'Nie ste prihlaseny do aplikacie, takze nevieme povedat ci ste prihlaseny na newsletter.',
    'is_subscribed' => 'Momentalne ste prihlaseny na newsletter.',
    'is_not_subscribed' => 'Momentalne nie ste prihlaseny na newsletter.',

    'subscribed' => 'Uspesne ste sa prihlasili na newsletter.',
    'unsubscribed' => 'Ste odhlaseny z newslettra',
];
