<?php


return [

    'email' => 'Email',
    'username' => 'Meno',
    'password' => 'Heslo',
    'confirm_password' => 'Heslo znova',

    'remember_me' => 'Pamätať si ma',
    'reset_password' => 'Obnoviť heslo',

    'do_send_reset_link' => 'Poslať link na obnovu emailom',
    'do_login' => 'Prihlásiť sa',
    'do_register' => 'Registrovať sa',

];
