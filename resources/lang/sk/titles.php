<?php
return [

    'greeting' => 'Ste prihlásený. Vitajte!',

    'login' => 'Prihlásenie',
    'logout' => 'Odhlasit sa',
    'register' => 'Registrácia',
    'newsletter' => 'Novinky emailom',
    'changepw' => 'Zmeniť heslo',

    'lang_slovak' => 'Slovensky',
    'lang_english' => 'Anglicky',

];
