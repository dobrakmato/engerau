<?php

return [

    'email' => 'Email address',

    'subscribe' => 'Subscribe',
    'unsubscribe' => 'Unsubscribe',
    'newsletter' => 'Newsletter',

    'notlogged' => 'You are not logged in, so we can\'t tell if you are subscribed.',
    'is_subscribed' => 'You are currently subscribed to our newsletter.',
    'is_not_subscribed' => 'You are currently not subscribed to our newsletter.',

    'subscribed' => 'You are subscribed to the newsletter.',
    'unsubscribed' => 'You are unsubscribed from the newsletter.',

];
