<?php
return [

    'greeting' => 'You are logged in. Welcome!',

    'login' => 'Login',
    'logout' => 'Logout',
    'register' => 'Registration',
    'newsletter' => 'Newsletter',
    'changepw' => 'Change password',

    'lang_slovak' => 'Slovak',
    'lang_english' => 'English',

];
