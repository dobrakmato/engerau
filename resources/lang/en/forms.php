<?php


return [

    'email' => 'Email',
    'username' => 'Username',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',

    'remember_me' => 'Remember me',
    'reset_password' => 'Reset password',

    'do_send_reset_link' => 'Send password reset link',
    'do_login' => 'Login',
    'do_register' => 'Register',

];
