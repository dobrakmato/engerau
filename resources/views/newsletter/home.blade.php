@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('titles.newsletter')</div>

                    <div class="panel-body">

                        @guest
                            <p>@lang('newsletter.notlogged')</p>
                        @else
                            @if ($is_subscribed)
                                <p style="color: green">@lang('newsletter.is_subscribed')</p>
                            @else
                                <p style="color: red;">@lang('newsletter.is_not_subscribed')</p>
                            @endif
                        @endguest

                        <h2>@lang('newsletter.subscribe')</h2>
                        <form class="form-horizontal" method="POST" action="{{ route('newsletter.subscribe') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">@lang('newsletter.email'):</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('newsletter.subscribe')
                                    </button>
                                </div>
                            </div>
                        </form>

                        <h2>@lang('newsletter.unsubscribe')</h2>
                        <form class="form-horizontal" method="POST" action="{{ route('newsletter.unsubscribe') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">@lang('newsletter.email'):</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('newsletter.unsubscribe')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
