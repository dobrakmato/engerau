Prihlasenie a registracia aj s resetom hesla tam je.

Newsletter je implementovany aj pre neprihlasenych ludi. Ak je user prihlaseny, vie si pozriet ci je subscribnuty alebo
nie. Registrujem ich do db a je tam aj nejaka integracia na MailChimp, tu som vsak netestoval - nemam API kluc. 
Pre jednoduchost su oba formy na jednej stranke v realite by sa to hodilo rozdelit - nie je to problem spravit.

Jazykove variace su nahodene tiez - v menu sa daju prepinat nie najlepsim riesenim, ale na ukazku to snad staci.

Na nasadenie netreba ziadne extra veci, iba to co vyzaduje Laravel by default. + nastavit .env