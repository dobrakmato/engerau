<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


/* Newsletters */
Route::get('/newsletter', 'NewsletterController@home')->name('newsletter.home');
Route::post('/newsletter/subscribe', 'NewsletterController@subscribe')->name('newsletter.subscribe');
Route::post('/newsletter/unsubscribe', 'NewsletterController@unsubscribe')->name('newsletter.unsubscribe');

Route::get('/newsletter/subscribed', 'NewsletterController@subscribed')->name('newsletter.subscribed');
Route::get('/newsletter/unsubscribed', 'NewsletterController@unsubscribed')->name('newsletter.unsubscribed');

/* Set locale */
Route::get('/locale/{locale}', 'LocaleController@setlocale')->name('setlocale');